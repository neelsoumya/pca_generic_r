# pca_generic_R

Generic function in R to perform PCA

	* Adapted from:

		http://www-bcf.usc.edu/~gareth/ISL/Chapter%2010%20Labs.txt
   
	* Installation:

		install_github("vqv/ggbiplot")
		install_github("kassambara/factoextra")



